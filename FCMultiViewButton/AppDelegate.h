//
//  AppDelegate.h
//  FCMultiViewButton
//
//  Created by aFrogleap on 11/6/12.
//  Copyright (c) 2012 farcoding. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
