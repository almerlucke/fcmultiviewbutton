//
//  FCMultiViewButton.h
//  FCMultiViewButton
//
//  Created by aFrogleap on 11/6/12.
//  Copyright (c) 2012 farcoding. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FCMultiViewButton : UIControl
@property (nonatomic, strong, readonly) UIView *contentView;
@property (nonatomic, strong) IBOutlet UIView *backgroundView;
@property (nonatomic, strong) IBOutlet UIView *highlightedBackgroundView;
@property (nonatomic, strong) IBOutlet UIView *highlightedOverlayView;
@end
