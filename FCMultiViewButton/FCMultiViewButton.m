//
//  FCMultiViewButton.m
//  FCMultiViewButton
//
//  Created by aFrogleap on 11/6/12.
//  Copyright (c) 2012 farcoding. All rights reserved.
//

#import "FCMultiViewButton.h"

@implementation FCMultiViewButton

@synthesize contentView = _contentView;

- (UIView *)contentView
{
    if (!_contentView) {
        _contentView = [[UIView alloc] initWithFrame:self.bounds];
        _contentView.backgroundColor = [UIColor clearColor];
        _contentView.userInteractionEnabled = NO;
        [self addSubview:_contentView];
    }
    
    return _contentView;
}

- (void)setBackgroundView:(UIView *)backgroundView
{
    _backgroundView = backgroundView;
    _backgroundView.userInteractionEnabled = NO;
    [self insertSubview:backgroundView atIndex:0];
}

- (void)setHighlightedBackgroundView:(UIView *)highlightedBackgroundView
{
    _highlightedBackgroundView = highlightedBackgroundView;
    highlightedBackgroundView.userInteractionEnabled = NO;
}

- (void)setHighlightedOverlayView:(UIView *)highlightedOverlayView
{
    _highlightedOverlayView = highlightedOverlayView;
    _highlightedOverlayView.userInteractionEnabled = NO;
}

- (void)addSubview:(UIView *)view
{
    if (view != _contentView &&
        view != _highlightedOverlayView &&
        view != _highlightedBackgroundView &&
        view != _backgroundView) {
        [self.contentView addSubview:view];
    } else {
        [super addSubview:view];
    }
}

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    
    if (!self.highlightedOverlayView) {
        if (highlighted) {
            if (self.highlightedBackgroundView && !self.highlightedBackgroundView.superview) {
                if (self.backgroundView) {
                    [self insertSubview:self.highlightedBackgroundView aboveSubview:self.backgroundView];
                } else {
                    [self insertSubview:self.highlightedBackgroundView atIndex:0];
                }
            }
        } else {
            if (self.highlightedBackgroundView && self.highlightedBackgroundView.superview) {
                [self.highlightedBackgroundView removeFromSuperview];
            }
        }
        
        for (id subview in self.contentView.subviews) {
            if ([subview respondsToSelector:@selector(setHighlighted:)]) {
                [subview setHighlighted:highlighted];
            }
        }
    } else {
        if (highlighted) {
            if (!self.highlightedOverlayView.superview) {
                [self addSubview:self.highlightedOverlayView];
            }
        } else {
            if (self.highlightedOverlayView.superview) {
                [self.highlightedOverlayView removeFromSuperview];
            }
        }
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.contentView.frame = self.bounds;
    if (self.backgroundView) self.backgroundView.frame = self.bounds;
    if (self.highlightedBackgroundView) self.highlightedBackgroundView.frame = self.bounds;
    if (self.highlightedOverlayView) self.highlightedOverlayView.frame = self.bounds;
}

@end
