//
//  ViewController.m
//  FCMultiViewButton
//
//  Created by aFrogleap on 11/6/12.
//  Copyright (c) 2012 farcoding. All rights reserved.
//

#import "ViewController.h"
#import "FCMultiViewButton.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()
@property (nonatomic, strong) IBOutlet FCMultiViewButton *customButton;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.customButton.layer.masksToBounds = YES;
    self.customButton.layer.cornerRadius = 10.0;
    self.customButton.highlightedBackgroundView.layer.cornerRadius = 10.0;
    self.customButton.backgroundView.layer.cornerRadius = 10.0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
