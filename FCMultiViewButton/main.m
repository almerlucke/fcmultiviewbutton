//
//  main.m
//  FCMultiViewButton
//
//  Created by aFrogleap on 11/6/12.
//  Copyright (c) 2012 farcoding. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
